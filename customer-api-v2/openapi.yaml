openapi: 3.0.0
info:
  title: OnDemand Customer API v2
  version: 1.0.0
  description: Swagger definition for Customer API v2.
  contact:
    email: novitas@colt.net
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
servers:
  # Added by API Auto Mocking Plugin
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/Haniorte/OnDemand-Customer-api-v2/1.0.0
tags:
  - name: Customer
    description: Endpoint for creating, updating & getting calls for customers.
  - name: PortalUser
    description: Endpoint for creating, updating & getting calls for portal users.

paths:
  /v1/customer:
    parameters:
      - name: "correlation_id"
        description: Correlates api calls accross microservices.
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"
    get:
      tags:
        - Customer
      summary: Gets a list of Customers by search criteria.
      description: This api will return a list of Customers according with search criteria.
      operationId: getCustomers
      parameters:
        - name: "name"
          description: Name of the Customer
          required: false
          in: query
          schema:
            type: string
          example: "AG Telecom Ltd UK"
        - name: "ocn"
          description: Ocn
          required: false
          in: query
          schema:
            type: string
          example: "100380"
        - name: "bcn"
          description: Bcn
          required: false
          in: query
          schema:
            type: string
          example: "250380"
        - name: "country_code"
          description: List of Country Codes
          required: false
          in: query
          schema:
            $ref: '#/components/schemas/CountryCodeV1'
          example: "GB"
        - name: "language_code"
          description: List of Language Codes
          required: false
          in: query
          schema:
            $ref: '#/components/schemas/SupportedLanguageV1'
          example: "EN"

      responses:
        "200":
          description: Success response
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CustomerResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

    post:
      tags:
        - Customer
      summary: Creates a new Customer.
      description: This api will create a new customer and will return the Customer created.
      operationId: createCustomer
      requestBody:
        description: Request of new Customer
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCustomerRequestV1'
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

  /v1/customer/{id}:
    parameters:
      - name: "id"
        description: Customer Id
        required: true
        in: path
        schema:
          type: integer
        example: 53139845
      - name: "correlation_id"
        description: Correlates api calls accross microservices
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"
    get:
      tags:
        - Customer
      summary: Gets a Customer by id.
      description: This api will return the Customer according with the id.
      operationId: getCustomerById
      responses:
        '200':
          description: Success response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerResponseV1'
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

    patch:
      tags:
        - Customer
      summary: Updates the attributes of the Customer with this id.
      description: This api will update the attributes of the Customer matching the id and will return the Customer with the new values updated.
      operationId: updateCustomer
      requestBody:
        description: Supply the new spend limit for the Customer
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchCustomerRequestV1'
      responses:
        '200':
          description: The update was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

  /v1/portal_user:
    parameters:
      - name: "correlation_id"
        description: Correlates api calls accross microservices
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"
    get:
      tags:
        - PortalUser
      summary: Gets a list of Portal Users by search criteria.
      description: This api will return a list of Portal Users according with search criteria.
      operationId: getUsers
      deprecated: true
      parameters:
        - name: "customer_id"
          description: Customer id
          required: false
          in: query
          schema:
            type: integer
          example: 53139845
        - name: "name"
          description: Name of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "Adan Grey"
        - name: "email"
          description: Email of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "adan@agtelecom.com"
        - name: "user_id"
          description: User id of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "randomuserid"
        - name: "user_status"
          description: Status of Portal User
          required: false
          in: query
          schema:
            $ref: '#/components/schemas/PortalUserStatusV1'
        - name: "customer_status"
          description: Status of Customer the Portal User belongs to
          required: false
          in: query
          schema:
            type: array
            items:
              $ref: '#/components/schemas/StatusV1'
      responses:
        "200":
          description: Success response
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PortalUserResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

    post:
      tags:
        - PortalUser
      summary: Creates new Portal User.
      description: This api will create a new Portal User and will return the Portal User created.
      operationId: createPortalUser
      deprecated: true
      requestBody:
        description: Request of the new Portal User
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreatePortalUserRequestV1'
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

  /v1/portal_user/{id}:
    parameters:
      - name: "id"
        description: Portal User ID
        required: true
        in: path
        schema:
          type: integer
        example: 67548
      - name: "correlation_id"
        description: Correlates api calls accross microservices
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"

    get:
      tags:
        - PortalUser
      summary: Gets a Portal User by id.
      description: This api will return the Portal User according with the id.
      operationId: getPortalUserById
      deprecated: true
      responses:
        '200':
          description: Success response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV1'
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

    patch:
      tags:
        - PortalUser
      summary: Updates the attributes of the Portal User with this id.
      description: This api will update the attributes of the Portal User matching the id and will return the Portal User with the new values updated.
      operationId: updatePortalUser
      deprecated: true
      requestBody:
        description: Supply the new preferred language for updating.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchPortalUserRequestV1'
      responses:
        '200':
          description: The update was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV1'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []
  /v2/portal_user:
    parameters:
      - name: "correlation_id"
        description: Correlates api calls accross microservices
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"
    get:
      tags:
        - PortalUser
      summary: Gets a list of Portal Users by search criteria.
      description: This api will return a list of Portal Users according with search criteria.
      operationId: getUsersV2
      parameters:
        - name: "customer_id"
          description: Customer id
          required: false
          in: query
          schema:
            type: integer
          example: 53139845
        - name: "name"
          description: Name of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "Adan Grey"
        - name: "email"
          description: Email of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "adan@agtelecom.com"
        - name: "user_id"
          description: User id of the Portal User
          required: false
          in: query
          schema:
            type: string
          example: "randomuserid"
        - name: "user_status"
          description: Status of Portal User
          required: false
          in: query
          schema:
            $ref: '#/components/schemas/PortalUserStatusV1'
        - name: "customer_status"
          description: Status of Customer the Portal User belongs to
          required: false
          in: query
          schema:
            type: array
            items:
              $ref: '#/components/schemas/StatusV1'
      responses:
        "200":
          description: Success response
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/PortalUserResponseV2'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []
    post:
      tags:
        - PortalUser
      summary: Creates new Portal User.
      description: This api will create a new Portal User and will return the Portal User created.
      operationId: createPortalUserv2
      requestBody:
        description: Request of the new Portal User
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreatePortalUserRequestV2'
      responses:
        "201":
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV2'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []
  /v2/portal_user/{id}:
    parameters:
      - name: "id"
        description: Portal User ID
        required: true
        in: path
        schema:
          type: integer
        example: 67548
      - name: "correlation_id"
        description: Correlates api calls accross microservices
        required: false
        in: header
        schema:
          type: string
        example: "1701fc2a-6a10-46e0-a3a1-ea161fc5123b"

    get:
      tags:
        - PortalUser
      summary: Gets a Portal User by id.
      description: This api will return the Portal User according with the id.
      operationId: getPortalUserByIdV2
      responses:
        '200':
          description: Success response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV2'
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []
    patch:
      tags:
        - PortalUser
      summary: Updates the attributes of the Portal User with this id.
      description: This api will update the attributes of the Portal User matching the id and will return the Portal User with the new values updated.
      operationId: updatePortalUserV2
      requestBody:
        description: Supply the new preferred language for updating.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchPortalUserRequestV2'
      responses:
        '200':
          description: The update was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PortalUserResponseV2'
        "400":
          description: Bad request
        "401":
          description: Authentication failure
        "404":
          description: Not Found
        "415":
          description: Unsupported Media Type
        "500":
          description: Internal server error
        "503":
          description: Service unavailable
      security:
        - basicAuth: []

components:
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic

  schemas:
    CommonGetCustomersResponseV1:
      type: object
      description: Customer Response when searching by attributes.
      required:
        - "id"
        - "status"
        - "name"
        - "ocn"
        - "bcn"
        - "service_id"
        - "country_code"
        - "language_code"
      properties:
        id:
          type: integer
          example: 53139845
        status:
          $ref: '#/components/schemas/StatusV1'
        name:
          type: string
          example: "AG Telecom Ltd UK"
        ocn:
          type: string
          example: "100380"
        bcn:
          type: string
          example: "250380"
        service_id:
          type: string
          example: "8000783"
        country_code:
          $ref: '#/components/schemas/CountryCodeV1'
        language_code:
          $ref: '#/components/schemas/SupportedLanguageV1'

    ColtRegionV1:
      description: List of Colt Regions.
      type: string
      example: "EU"
      enum:
        - "EU"
        - "ASIA"

    CountryV1:
      description: List of countries.
      type: string
      example: "UNITED KINGDOM"
      enum:
        - "AUSTRALIA"
        - "AUSTRIA"
        - "BELGIUM"
        - "BULGARIA"
        - "CANADA"
        - "CROATIA"
        - "CZECH REPUBLIC"
        - "DENMARK"
        - "FINLAND"
        - "FRANCE"
        - "GERMANY"
        - "HONG KONG"
        - "HUNGARY"
        - "IRELAND"
        - "ITALY"
        - "JAPAN"
        - "LUXEMBOURG"
        - "NETHERLANDS"
        - "POLAND"
        - "PORTUGAL"
        - "ROMANIA"
        - "SERBIA"
        - "SINGAPORE"
        - "SLOVAKIA"
        - "SPAIN"
        - "SWEDEN"
        - "SWITZERLAND"
        - "UNITED KINGDOM"
        - "UNITED STATES"

    CountryCodeV1:
      description: List of Countries Codes.
      type: string
      example: "GB"
      enum:
        - "AT"
        - "AU"
        - "BE"
        - "BG"
        - "CA"
        - "CH"
        - "CS"
        - "CZ"
        - "DE"
        - "DK"
        - "ES"
        - "FI"
        - "FR"
        - "GB"
        - "HR"
        - "HK"
        - "HU"
        - "IE"
        - "IT"
        - "JP"
        - "LU"
        - "NL"
        - "PL"
        - "PT"
        - "RO"
        - "SG"
        - "SK"
        - "SE"
        - "US"

    CreateCustomerRequestV1:
      description: New Customer Request.
      type: object
      required:
        - "name"
        - "local_name"
        - "ocn"
        - "bcn"
        - "currency"
        - "country_code"
        - "language_code"
      properties:
        name:
          type: string
          example: "AG Telecom Ltd UK"
        local_name:
          type: string
          example: "AG Telecom Ltd"
        ocn:
          type: string
          example: "100380"
        bcn:
          type: string
          example: "250380"
        currency:
          type: string
          example: "GBP"
        country_code:
          $ref: '#/components/schemas/CountryCodeV1'
        language_code:
          $ref: '#/components/schemas/SupportedLanguageV1'
    
    CreatePortalUserRequestV1:
      description: New Portal User Request.
      type: object
      required:
        - "customer_id"
        - "username"
        - "name"
        - "title"
        - "email"
        - "telephone_number"
        - "preferred_language"
        - "most_recent_login_dt"
        - "most_recent_user_role"
        - "user_type"
        - "are_concurrent_sessions_allowed"
        - "user_hash"
      properties:
        customer_id:
          type: integer
          example: 53139845
        username:
          type: string
          example: "adangrey"
        name:
          type: string
          example: "Adan Grey"
        title:
          type: string
          example: "Mr"
        email:
          type: string
          example: "adangrey@agtelecom.com"
        telephone_number:
          type: string
          example: "+44123456789"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        user_type:
          $ref: "#/components/schemas/UserTypeV1"
        are_current_sessions_allowed:
          type: boolean
          example: true
        user_hash:
          type: string
          example: "574b4251593d"

    CreatePortalUserRequestV2:
      description: New Portal User Request.
      type: object
      required:
        - "associated_customers"
        - "username"
        - "name"
        - "title"
        - "email"
        - "telephone_number"
        - "preferred_language"
        - "most_recent_login_dt"
        - "most_recent_user_role"
        - "user_type"
        - "are_concurrent_sessions_allowed"
        - "user_hash"
        - "preferred_customer_id"
      properties:
        username:
          type: string
          example: "adangrey"
        name:
          type: string
          example: "Adan Grey"
        title:
          type: string
          example: "Mr"
        email:
          type: string
          example: "adangrey@agtelecom.com"
        telephone_number:
          type: string
          example: "+44123456789"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        user_type:
          $ref: "#/components/schemas/UserTypeV1"
        are_current_sessions_allowed:
          type: boolean
          example: true
        user_hash:
          type: string
          example: "574b4251593d"
        associated_customers:
          type: array
          description: "Customer's OCN"
          items:
            type: string
            example: "AC121"
        preferred_customer_ocn:
          type: string
          example: "ACO242"
    CurrencyV1:
      description: List of Currencies.
      type: string
      example: "GBP"
      enum:
        - "CHF"
        - "DKK"
        - "EUR"
        - "GBP"
        - "HKD"
        - "JPY"
        - "NOK"
        - "SEK"
        - "SGD"
        - "USD"

    CustomerResponseV1:
      description: Customer Response.
      type: object
      required:
        - "id"
        - "status"
        - "name"
        - "local_name"
        - "ocn"
        - "bcn"
        - "currency"
        - "is_spend_limited"
        - "monthly_spend_limit"
        - "colt_region"
        - "country_code"
        - "country"
        - "language_code"
        - "ocn_hash"
        - "performance_tier"
        - "is_mfa_required"
      properties:
        id:
          type: integer
          example: 53139845
        status:
          $ref: '#/components/schemas/StatusV1'
        name:
          type: string
          example: "AG Telecom Ltd UK"
        local_name:
          type: string
          example: "AG Telecom Ltd"
        ocn:
          type: string
          example: "100380"
        bcn:
          type: string
          example: "250380"
        currency:
          $ref: '#/components/schemas/CurrencyV1'
        is_spend_limited:
          type: boolean
          example: true
        monthly_spend_limit:
          type: number
          format: double
          example: 1000.8
        colt_region:
          $ref: '#/components/schemas/ColtRegionV1'
        country_code:
          $ref: '#/components/schemas/CountryCodeV1'
        country:
          $ref: '#/components/schemas/CountryV1'
        language_code:
          $ref: '#/components/schemas/SupportedLanguageV1'
        ocn_hash:
          type: string
          example: "663032556b3d"
        performance_tier:
          type: string
          example: "A"
        is_mfa_required:
          type: boolean
          example: true
        city:
          type: string
          example: "London"

    PatchCustomerRequestV1:
      description: Request for updating Customer attributes.
      type: object
      properties:
        status:
          $ref: '#/components/schemas/StatusV1'
        is_spend_limited:
          type: boolean
          example: true
        monthly_spend_limit:
          type: number
          format: double
          example: 1000.8
        country_code:
          $ref: '#/components/schemas/CountryCodeV1'
        language_code:
          $ref: '#/components/schemas/SupportedLanguageV1'
        ocn_hash:
          type: string
          example: "663032556b3d"

    PatchPortalUserRequestV1:
      description: Request for updating Portal User attributes.
      type: object
      properties:
        status:
          $ref: "#/components/schemas/PortalUserStatusV1"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        tc_accepted_dt:
          type: string
          example: "21-FEB-19"
        tc_accepted_id:
          type: integer
          example: 456329
        customer_id:
          type: integer
        user_hash:
          type: string
        reset_tc:
          type: boolean
          
    PatchPortalUserRequestV2:
      description: Request for updating Portal User attributes.
      type: object
      properties:
        status:
          $ref: "#/components/schemas/PortalUserStatusV1"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        user_hash:
          type: string
        reset_tc:
          type: boolean
        preferred_customer_ocn:
          type: string
          example: "AC1213"
        associated_customers:
          type: array
          description: "Customer's OCN"
          items:
            type: string
            example: "AC121"
        tc_accepted_id:
          type: integer
          example: 2323

    PortalUserResponseV1:
      description: Portal User Response.
      type: object
      required:
        - "id"
        - "customer_id"
        - "username"
        - "status"
        - "name"
        - "title"
        - "email"
        - "telephone_number"
        - "preferred_language"
        - "most_recent_login_dt"
        - "most_recent_user_role"
        - "tc_accepted_dt"
        - "tc_accepted_id"
        - "user_type"
        - "are_concurrent_sessions_allowed"
        - "user_hash"
        - "api_user"
        - "multi_session"
      properties:
        id:
          type: integer
          example: 67548
        customer_id:
          type: integer
          example: 53139845
        username:
          type: string
          example: "adangrey"
        status:
          $ref: "#/components/schemas/PortalUserStatusV1"
        name:
          type: string
          example: "Adan Grey"
        title:
          type: string
          example: "Mr"
        email:
          type: string
          example: "adangrey@agtelecom.com"
        telephone_number:
          type: string
          example: "+44123456789"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        tc_accepted_dt:
          type: string
          example: "21-FEB-19"
        tc_accepted_id:
          type: integer
          example: 456329
        user_type:
          $ref: "#/components/schemas/UserTypeV1"
        are_concurrent_sessions_allowed:
          type: boolean
          example: true
        user_hash:
          type: string
          example: "574b4251593d"
        api_user:
          type: boolean
        multi_session:
          type: boolean

    PortalUserResponseV2:
      description: Portal User Response.
      type: object
      required:
        - "id"
        - "username"
        - "status"
        - "name"
        - "title"
        - "email"
        - "telephone_number"
        - "preferred_language"
        - "most_recent_login_dt"
        - "most_recent_user_role"
        - "user_type"
        - "are_concurrent_sessions_allowed"
        - "user_hash"
        - "api_user"
        - "multi_session"
        - "associated_customers"
        - "preferred_customer_id"
      properties:
        id:
          type: integer
          example: 67548
        username:
          type: string
          example: "adangrey"
        status:
          $ref: "#/components/schemas/PortalUserStatusV1"
        name:
          type: string
          example: "Adan Grey"
        title:
          type: string
          example: "Mr"
        email:
          type: string
          example: "adangrey@agtelecom.com"
        telephone_number:
          type: string
          example: "+44123456789"
        preferred_language:
          $ref: "#/components/schemas/SupportedLanguageV1"
        most_recent_login_dt:
          type: string
          example: "12-DEC-20"
        most_recent_user_role:
          $ref: "#/components/schemas/UserRoleV1"
        user_type:
          $ref: "#/components/schemas/UserTypeV1"
        are_concurrent_sessions_allowed:
          type: boolean
          example: true
        user_hash:
          type: string
          example: "574b4251593d"
        api_user:
          type: boolean
        multi_session:
          type: boolean
        associated_customers:
          type: array
          items:
            $ref: "#/components/schemas/AssociatedCustomersV1"
        preferred_customer_id:
          type: integer
          example: 1212
            
    PortalUserStatusV1:
      description: Status for Portal User
      type: string
      example: "ACTIVE"
      enum:
        - "ACTIVE"
        - "ARCHIVED"

    StatusV1:
      description: Status of the Customer |
        | ONBOARDING -> The OnDemand Customer record is not yet active.
        | SIEBEL_ONBOARDING -> The OnDemand Customer record is waiting for a Siebel umbrella service ID
        | KENAN_ONBOARDING -> The OnDemand Customer record is waiting for confirmation that billing charges generated by OnDemand will be processed correctly.
        | ONBOARD_FAILED -> Creation of the OnDemand Customer record via the Admin UI failed. Please check Admin API logs.
        | ACTIVE -> The OnDemand Customer record for this user is active and ready to accept requests
        | LOCKED -> The OnDemand Customer record for this user has been locked and no further logins are allowed.
        | INACTIVE -> The OnDemand Customer record for this user has been marked as Inactive and needs manual intervention to update it.
        | ARCHIVED -> The OnDemand Customer record for this user has been archived and cannot be restored. No requests will be accepted and all services have been ceased.
        | PENDING -> The OnDemand Customer record for this user has not yet been fully created. Try again later.
        | SOFTCEASE -> A user of the Admin UI has requested that the OnDemand Customer record for this user should be "soft-ceased". This is usually for billing-related reasons. Services will be ceased and no new orders will be accepted. The Customer record will be set to INACTIVE and some time later all services will be irrevocably ceased and the Customer record will be ARCHIVED.
        | SOFTCEASE_FAILED->  An Admin UI user tried to soft-cease the Customer record for this user but this action failed. No new requests will be accepted. Services may be in an inconsistent state.
        | SOFTCEASE_INPROGRESS -> An Admin UI user requested a "soft-cease" on the Customer account for this user, and services are being ceased.
        | SOFTRESTORE -> An Admin UI user requested the "soft- cease" on the Customer account for this user be removed. An attempt will be made to restore previously ceased services. When that is complete, the Customer Status will be set to ACTIVE.
        | SOFTRESTORE_FAILED -> When moving the Customer record for this user from INACTIVE to ACTIVE, one or more services could not be restored.
        | SOFTRESTORE_INPROGRESS -> A user of the Admin UI has requested restoration of the Customer account for this user. Services that were previously "soft-ceased" are being restored.
        | HARDCEASE -> A user of the Admin UI has requested permanent cessation of all services for this user's Customer account. When complete, the Customer record will be marked as ARCHIVED.
        | HARDCEASE_FAILED -> Something went wrong when trying to permanently cease all services for this user's Customer account. Some services may have been permanently ceased while others may remain in a "soft-cease" state.
        | HARDCEASE_INPROGRESS -> Services for this user's Customer account are being permanently ceased.
      type: string
      example: "ACTIVE"
      enum:
        - "ONBOARDING"
        - "SIEBEL_ONBOARDING"
        - "KENAN_ONBOARDING"
        - "ONBOARD_FAILED"
        - "ACTIVE"
        - "LOCKED"
        - "INACTIVE"
        - "ARCHIVED"
        - "PENDING"
        - "SOFTCEASE"
        - "SOFTCEASE_FAILED"
        - "SOFTCEASE_INPROGRESS"
        - "SOFTRESTORE"
        - "SOFTRESTORE_FAILED"
        - "SOFTRESTORE_INPROGRESS"
        - "HARDCEASE"
        - "HARDCEASE_FAILED"
        - "HARDCEASE_INPROGRESS"

    SupportedLanguageV1:
      description: Languages supported for the Portal User.
      type: string
      example: "EN"
      enum:
        - "DE"
        - "EN"
        - "ES"
        - "FR"
        - "IT"
        - "JA"

    UserRoleV1:
      description: Portal User roles.
      type: string
      example: "OnDemandReadOnlyRole"
      enum:
        - "OnDemandFullRole"
        - "OnDemandFlexRole"
        - "OnDemandPrimaryRole"
        - "OnDemandReadOnlyRole"

    UserTypeV1:
      description: Portal User types.
      type: string
      example: "super_user"
      enum:
        - "impersonate_user"
        - "super_user"
        
    AssociatedCustomersV1:
      type: object
      required:
        - customer_id
        - preferred_customer
      properties:
        customer_id:
          type: integer
          example: 1212
        legal_name:
          type: string
          example: "ABC Ltd"
        ocn:
          type: string
          example: AC1212
        bcn:
          type: string
          example: 1213
        tc_accepted_id:
          type: integer
          example: 3113
        tc_accepted:
          type: boolean
          example: true
        tc_accepted_dt:
          type: string
          format: date-time
