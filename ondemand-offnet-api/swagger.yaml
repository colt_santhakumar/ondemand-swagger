openapi: 3.0.1
info:
  title: Arnot Service
  description: Rest microservice to call Arnot rest endpoints related to offnet Port price
  version: "0.1"
tags:
  - name: Offnet Port
  - name: ULL

paths:
  /v1/offnet_candidates:
    get:
      tags:
        - Offnet Port
      summary: Get offnet port prices from Arnot for multiple OLOs.
      description: Get offnet port prices from Arnot for multiple OLOs.
      operationId: getOffnetPortPrices
      parameters:
        - in: query
          name: batch_id
          schema:
            type: integer
          required: true
          example: 1
        - in: query
          name: street_name
          schema:
            type: string
          required: true
          example: Great Eastern Street
        - in: query
          name: premises_number
          schema:
            type: string
          required: false
          example: 20
        - in: query
          name: city
          schema:
            type: string
          required: true
          example: London
        - in: query
          name: postcode
          schema:
            type: string
          required: false
          example: EC2A 3EH
        - in: query
          name: country
          schema:
            type: string
          required: true
          example: United Kingdom
        - in: query
          name: bandwidth_mbps
          schema:
            type: array
            items:
              type: integer
            minItems: 1
            maxItems: 5
            uniqueItems: true
          required: true
          example: [100, 200, 300]
        - in: query
          name: latitude
          schema:
            type: number
            format: float
          required: true
          example: 48.82575573
        - in: query
          name: longitude
          schema:
            type: number
            format: float
          required: true
          example: 2.323917522
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ArnotOffnetResponseV1'

  /v1/building/ull_fibre:
    get:
      tags:
        - ULL
      summary: Get List of ULL Fibre Buildings from CPQ system.
      description: Get List of ULL Fibre Buildings from CPQ system.
      operationId: getUllFibreBuildings
      parameters:
        - in: query
          name: country
          required: true
          schema:
            type: string
          example: Japan
        - in: query
          name: city
          schema:
            type: string
          required: true
          example: NAGOYA SHI MIDORI KU
        - in: query
          name: street_name
          schema:
            type: string
          required: true
          example: ODAKACHO
        - in: query
          name: postcode
          schema:
            type: string
          required: false
          example: 4598001
        - in: query
          name: building_name
          schema:
            type: string
          required: false
          example: Nakacho
        - in: query
          name: building_number
          schema:
            type: string
          required: false
          example: 49
        - in: query
          name: latitude
          schema:
            type: number
            format: float
            description: Latitude of the building location
        - in: query
          name: longitude
          schema:
            type: number
            format: float
            description: Longitute of the building location
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ULLFibreResponseV1'


  /v1/building/ull_fibre/{building_id}:
    get:
      tags:
        - ULL
      summary: Get ULL Fibre Building from building_id.
      description: Get ULL Fibre Building from building_id.
      operationId: getUllFibreBuildingById
      parameters:
        - in: path
          name: building_id
          required: true
          schema:
            type: integer
          example: 61344
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ULLFibreResponseV1'

components:
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
  schemas:
    ArnotOffnetResponseV1:
      type: object
      properties:
        batch_id:
          type: integer
          example: 1
        quote_id:
          type: string
          example: 202207211010181-1
        supplier:
          type: string
          example: Orange
        supplier_product:
          type: string
          example: CELAN
        country:
          type: string
          example: United Kingdom
        nni:
          type: string
          example: FRMAL-0000013026-FR-23 RUE PIERRE VALETTE-MALAKOFF-92240- CELAN_Malakoff_6_10G_ IDF-SRTHD4_Malakoff_XBT/XBT/LE-254246
        nni_site_type:
          type: string
        cpe_required:
          type: boolean
        cpe_permitted:
          type: boolean
        mrc:
          type: number
          example: 450
        nrc:
          type: number
          example: 1500
        currency:
          type: string
          example: GBP
        commitment_months:
          type: integer
          example: 12
        bandwidth_mbps:
          type: integer
          example: 200
        notice_period_months:
          type: integer
          example: 3
      required:
        - batch_id
        - quote_id
        - supplier
        - supplier_product
        - country
        - nni
        - nni_site_type
        - cpe_required
        - cpe_permitted
        - mrc
        - nrc
        - currency
        - commitment_months
        - bandwidth_mbps
        - notice_period_months

    ULLFibreResponseV1:
      type: object
      description: Object for ULL Fibre Response
      properties:
        building_id:
          type: integer
          example: 61344
        country:
          type: string
          example: Japan
        city:
          type: string
          example: NAGOYA SHI MIDORI KU
        postcode:
          type: string
          example: 4598001
        address:
          type: string
          example: NAGOYA SHI MIDORI KU, Japan, 4598001
        max_allowed_bandwidth:
          type: integer
          example: 200
        bmo_installation_charge:
          type: number
          format: float
          example: 2456.1
        bmo_recurring_charge:
          type: number
          format: float
          example: 342.1
        bmo_currency:
          type: string
        building_category:
          type: string
          example: Standard Building
        latitude:
          type: number
          format: float
        longitude:
          type: number
          format: float

    ErrorResponse:
      type: object
      description: The generic error response object.
      properties:
        code:
          description: The api internal error code.
          example: OO_EC1
          type: string
        internal_message:
          description: A message stating the reason for the error. This is meant for internal use only.
          example: Unexpected error.
          type: string
        message:
          description: A message stating the reason for the error. This can be shown to a user.
          example: Something went wrong. Please try again later or contact COLT customer support.
          type: string
      required:
        - code
        - internal_message
        - message
security:
  - basicAuth: []
