openapi: '3.0.2'
info:
  title: OnDemand Itential wrapper microservice
  version: '0.1'
  contact:
    name: Simon Farrell
    email: 'simon.farrell@colt.net'
  description: This file defines the notification service that Itential should call to update job status and to pass back information when a particular workflow completes.
tags:
  - name: internal
    description: methods called internally by the OnDemand application
  - name: external
    description: methods called by the Itential application
servers:
  - url: /simon/itential-notification/v1
paths:
  /wrapper/net_create:
    post:
      operationId: netCreate
      requestBody:
        description: defines the service ID and network details for CVIM ports that will be attached to a VNF
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NetCreateRequest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JobResponse'
        '400':
          description: Invalid payload
      summary: the payload will be enriched and passed on to Itential
      tags:
        - internal

  /wrapper/vnf_create:
    post:
      operationId: vnfCreate
      requestBody:
        description: defines attributes required for VNF creation
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VNFCreateRequest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JobResponse'
        '400':
          description: Invalid payload
      summary: the payload will be enriched and passed on to Itential
      tags:
        - internal

  /wrapper/net_delete:
    post:
      operationId: netDelete
      requestBody:
        description: Delete CVIM ports that were attached to a VNF
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NetDeleteRequest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JobResponse'
        '400':
          description: Invalid payload
      summary: the payload will be enriched and passed on to Itential
      tags:
        - internal

  /wrapper/vnf_delete:
    post:
      operationId: vnfDelete
      requestBody:
        description: Delete a VNF
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VNFDeleteRequest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JobResponse'
        '400':
          description: Invalid payload
      summary: the payload will be enriched and passed on to Itential
      tags:
        - internal

  /wrapper/mcr_config:
    post:
      operationId: mcrConfig
      requestBody:
        description: defines interface IP addresses and static routes for a VNF
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/MCRConfigRequest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JobResponse'
        '400':
          description: Invalid payload
      summary: the payload will be enriched and passed on to Itential
      tags:
        - internal

  /notification/itential_job:
    post:
      operationId: handleItentialNotification
      requestBody:
        description: Includes Job ID, status and then a job-specific payload
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NotificationRequest'
      responses:
        '200':
          description: OK
        '400':
          description: Invalid payload
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/NotificationError'
      summary: Name/value pairs for specific jobs will be agreed outside of this API definition
      tags:
        - external

components:
  schemas:
    JobResponse:
      description: Response for all job endpoints
      properties:
        itential_job_id:
          type: string
    NotificationRequest:
      description: A payload sent by Itential to the upstream system that describes the state of a job
      properties:
        data:
          description: a set of values that can be used to pass data from Itential to OnDemand. For now, only a FAILED job_status required an ERROR item in this array. All other statuses will be accepted with no accompanying data array.
          type: array
          items:
            $ref: '#/components/schemas/NameValueItem'
        job_id:
          type: string
        status:
          $ref: '#/components/schemas/JobStatus'
      required:
        - job_id
        - status
      type: object
    JobStatus:
      description: the state of a Job in Itential. Itential should send ERROR if there is a problem that might be fixed, followed by RESUME if the problem was fixed and no action is required by OnDemand, RETRY if the problem was fixed but the workflow should be re-invoked and FAILED if the problem could not be resolved.
      enum:
        - COMPLETED
        - ON HOLD
        - RESUME
        - FAIL
        - NON EXIST
        - CREATED
      type: string
    Interface:
      description: a Unix-like interface definition
      type: object
      properties:
        lan:
          $ref: '#/components/schemas/MaskedIPv4'
        lan_old:
          $ref: '#/components/schemas/MaskedIPv4'
        wan:
          $ref: '#/components/schemas/MaskedIPv4'
    Location:
      description: NFVI pod Location in the form <city-code>-pod<id>
      example: lon-pod1
      type: string
    MCRConfigRequest:
      description: Properties required for Itential to configure a managed router
      properties:
        request_id:
          description: the OnDemand request ID that originated the message
          type: string
        service_id:
          $ref: '#/components/schemas/ServiceID'
        vnf_name:
          $ref: '#/components/schemas/VNFName'
        interfaces:
          type: array
          items:
            $ref: '#/components/schemas/Interface'
        routing_options:
          type: array
          items:
            $ref: '#/components/schemas/StaticRoute'
      required:
        - request_id
        - service_id
        - vnf_name
    MaskedIPv4:
      description: IPv4 address  with a mask
      example: 10.0.0.1/16
      type: string
    NameValueItem:
      properties:
        name:
          type: string
        value:
          type: string
      required:
        - name
        - value
      type: object
    NetCreateRequest:
      description: Properties required for Itential to create an SRIOV port
      properties:
        location:
          $ref: '#/components/schemas/Location'
        request_id:
          description: the OnDemand request ID that originated the message
          type: string
        service_id:
          $ref: '#/components/schemas/ServiceID'
        network1_name:
          $ref: '#/components/schemas/NetworkName'
        network2_name:
          $ref: '#/components/schemas/NetworkName'
        network1_vlan_id:
          $ref: '#/components/schemas/VLANID'
        network2_vlan_id:
          $ref: '#/components/schemas/VLANID'
      required:
        - location
        - request_id
        - service_id
        - network1_name
        - network2_name
        - network1_vlan_id
        - network2_vlan_id
      type: object
    NetDeleteRequest:
      description: Properties required for Itential to create an SRIOV port
      properties:
        location:
          $ref: '#/components/schemas/Location'
        request_id:
          description: the OnDemand request ID that originated the message
          type: string
        service_id:
          $ref: '#/components/schemas/ServiceID'
        network1_name:
          $ref: '#/components/schemas/NetworkName'
        network2_name:
          $ref: '#/components/schemas/NetworkName'
        network1_vlan_id:
          $ref: '#/components/schemas/VLANID'
        network2_vlan_id:
          $ref: '#/components/schemas/VLANID'
      required:
        - location
        - request_id
        - service_id
        - network1_name
        - network2_name
        - network1_vlan_id
        - network2_vlan_id
      type: object
    NetworkName:
      description: Unique network name
      example: LON/LON/LU-12345
      type: string
    NotificationError:
      description: Something prevented the OnDemand microservice from handling the notification
      properties:
        error_code:
          description: placeholder pending agreement with Itential about how to handle errors returned by this service
          type: integer
        error_message:
          type: string
        job_id:
          type: string
      required:
        - error_message
        - job_id
      type: object
    ServiceID:
      description: Siebel identifier for the customer service
      example: 8000123
      type: string
    StaticRoute:
      description: a static route for IPv4 addresses
      properties:
        delete:
          description: set true if you want to delete the route that matches the destination_subnet
          type: boolean
          default: false
        destination_subnet:
          type: string
          minLength: 9
          maxLength: 18
        next_hop:
          type: string
          format: ipv4
      type: object
    VLANID:
      description: Unique identifier for a customer LAN or WAN connection on the TOR LAG. Range from 101 to 3900
      example: 101
      type: integer
    VNFCreateRequest:
      description: Properties required for Itential to create a VNF
      properties:
        location:
          $ref: '#/components/schemas/Location'
        request_id:
          description: the OnDemand request ID that originated the message
          type: string
        service_id:
          $ref: '#/components/schemas/ServiceID'
        vnf_name:
          $ref: '#/components/schemas/VNFName'
        vnf_type:
          $ref: '#/components/schemas/VNFType'
        vnf_size:
          $ref: '#/components/schemas/VNFSize'
        colt_mgmt:
          type: boolean
        mgmt_ip:
          $ref: '#/components/schemas/MaskedIPv4'
        mgmt_network:
          type: string
        networks:
          type: array
          items:
            type: string
      required:
        - location
        - request_id
        - service_id
        - vnf_name
        - vnf_type
        - vnf_size
        - colt_mgmt
        - mgmt_ip
        - mgmt_network
    VNFDeleteRequest:
      description: Properties required for Itential to create a VNF
      properties:
        location:
          $ref: '#/components/schemas/Location'
        request_id:
          description: the OnDemand request ID that originated the message
          type: string
        service_id:
          $ref: '#/components/schemas/ServiceID'
        vnf_name:
          $ref: '#/components/schemas/VNFName'
      required:
        - location
        - request_id
        - service_id
        - vnf_name
    VNFName:
      description: name of the VNF in the form mcr_<ServiceID>_<asset-id>
      example: mcr_8001234_65478915
      type: string
    VNFSize:
      description: description of the size of the service (bandwidth based)
      example: 100M or 1G
      type: string
    VNFType:
      description: description related to the service
      example: MCR
      type: string
    ErrorResponse:
      properties:
        error_detail:
          type: array
          items:
            $ref: '#/components/schemas/ErrorDetail'

    ErrorDetail:
      properties:
        message:
          $ref: '#/components/schemas/Message'
        node:
          type: string
        wf_name:
          type: string
    Message:
      properties:
        status:
          type: string
        error_detail:
          type: array
          items:
            $ref: '#/components/schemas/ErrorDetail'
    VNFConfigDataResponse:
      properties:
        routes:
          type: array
          items:
            $ref: '#/components/schemas/VNFConfigDataRoute'
        interfaces:
          type: array
          items:
            $ref: '#/components/schemas/VNFConfigDataInterface'
    VNFConfigDataRoute:
      properties:
        headers:
          type: array
          items:
            type: string
        mapped:
          type: array
          items:
            $ref: '#/components/schemas/VNFConfigDataRoutesMapped'
        unmapped:
          type: array
          items:
            type: array
            items:
              type: string
        name:
          type: string
    VNFConfigDataInterface:
      properties:
        headers:
          type: array
          items:
            type: string
        mapped:
          type: array
          items:
            $ref: '#/components/schemas/VNFConfigDataInterfacesMapped'
        unmapped:
          type: array
          items:
            type: array
            items:
              type: string
        name:
          type: string
    VNFConfigDataRoutesMapped:
      properties:
        DESTINATION:
          type: string
        NEXT_HOP:
          type: string
    VNFConfigDataInterfacesMapped:
      properties:
        INTERFACE:
          type: string
        LINK_STATUS:
          type: string
        ADMIN_STATE:
          type: string
        ADDRESS:
          type: string

  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
security:
  - basicAuth: []
